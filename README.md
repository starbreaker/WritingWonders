# The Authors of #WritingWonders

## Introduction

#WritingWonders is a hashtag on the Fediverse where a community of writers meets to discuss daily prompts and questions about their work.  
It is organised by [Alina Leonova](https://wandering.shop/@AlinaLeonova), [Branwen OShea](https://writing.exchange/@BranwenOShea) and [Amelia Kayne](https://writing.exchange/@AmeliaKayne). They publish a new set of questions every month.
An example for March 2023 can be found [here](https://cdn.masto.host/writing/media_attachments/files/109/931/636/776/047/458/original/7311dc6f090a252c.jpg).

I wanted to get a better overview of all the authors involved in this fun endeavour, so I asked the community to provide some basic information about them and their writings, and especially a good starting point to get to know them and their work.

Below is a list, in alphabetical order, of some of the participants who agreed to take part. If you are looking for something to add to your to-read pile, look no further and enjoy who or what you might discover here.

## Authors

### Alina Leonova

* Website: https://alinaleonova.net
* Fediverse Profile: https://wandering.shop/@AlinaLeonova
* Genre: Sci-Fi
* Start reading here: Entanglement: A Dystopian Sci-Fi Thriller - https://alinaleonova.net/book
    * New book "Wild Flowers, Electric Beasts" is coming soon.

### Amelia Kayne

* Website: https://ameliakayne.com
* Fediverse Profile: https://writing.exchange/@AmeliaKayne
* Genre: Romance
* Start reading here: See blogs on website

### Angela Miller

* Website: https://crowkitchentales.wordpress.com
* Fediverse Profile: https://mastodon.scot/@Alternatecelt
* Genre: Fantasy and Poetry, with a bit of Sci-Fi and Horror too
* Start reading here:
    * Website is her main writing and poetry blog with excerpts of her work.
    * Side blog [Tapestries of the Veils](https://tapestriesoftheveils.wordpress.com) is dedicated to the series she is currently working on.

### Benjamin Cox

* Website: https://hubpages.com/@benjamincox
* Fediverse Profile: https://mastodon.world/@benjamincox
* Genre: Screenplays and film critique
* Start reading here: 
    * Most recent review: [John Wick: Chapter 4](https://discover.hubpages.com/entertainment/Should-I-Watch-John-Wick-Chapter-4-2023)
    * He also compiles lists of interesting film trivia such as a list of supposed movie curses: [Top 20 Movie Curses](https://discover.hubpages.com/entertainment/Top-20-Movie-Curses)

### C. R. Collins

* Website: https://crcollins.org
* Fediverse Profile: https://writing.exchange/@crcollins
* Genre: Fantasy
* Start reading here: Woodspell Volume 1 in the Woodspell Series
    * Spoiler warning. Tales of Ardonna books are either sequels or prequels to the Woodspell trilogy. They are standalones and can be read in any order, if one doesn't mind spoilers.

### Christiane Knight

* Website: https://www.christianeknight.com
* Fediverse Profile: https://wandering.shop/@xiane
* Genre: Contemporary fantasy
* Start reading here: [In Sleep You Know](https://www.christianeknight.com/in-sleep-you-know/)
    * First book of an open ended series, here the reader meets everyone in the crew.

### Christina Anne Hawthorne

* Website: https://christinahawthorne.wordpress.com/
* Fediverse Profile: https://writing.exchange/@CA_Hawthorne
* Genre: Otherworld historical fantasy
* Start reading here: Has her website since 2013 & posts religiously each Thursday.
    * She writes otherworld historical fantasy in Ontyre. The Kovenlore Chronicles series in Carrdia is drafted. It's 1890s-esque & dystopian, the 7 novels each an adventure. Planned to be self-published in 2024.
    * After that, there'll be the Pannulus Mysteries & Thornwillow Tales, both 1920s-esque (some are drafted). 

### Finn McLellan

* Website: https://saranadosfiction.com/
* Fediverse Profile: https://wandering.shop/@house_of_five
* Genre: Queer gaslamp fantasy
* Start reading here: The trilogy [Argentum In Aqua](https://saranadosfiction.com/blood-on-the-snow-argentum-in-aqua-1-index/)

### Hannah Steenbock

* Website: https://www.hannah-steenbock.de/
* Fediverse Profile: https://writing.exchange/@Firlefanz
* Genre: Fantasy, SF
* Start reading here:
     * There are four different series (three complete) in different genres, so pick a genre and start with the 1st one:
        * The Cloudlands Saga (dragons, fantasy setting, low magic)
        * The Franssisi Four Chronicles (SF, steamy, alien colony with human slaves)
        * Wolves of the South (Urban Fantasy, not steamy, adventure with romance)
        * Winds of Destiny (epic fantasy, fight for freedom)

### Jocelin Sordoni

* Website: https://josordoni.co.uk
* Fediverse Profile: https://writing.exchange/@Josordoni
* Genre: Fantasy
* Start reading here:
    * Draft of [How To Breathe Underwater](https://archiveofourown.org/users/josordoni)
    * [Barky, The Space Wolf](https://josordoni.co.uk/wp/stories/) - Available in monthly episodes in story section on the website

### John Howes

* Website: https://johnahowes.blogspot.com
* Fediverse Profile: https://mastodonapp.uk/@johnhowesauthor
* Genre: Science fiction and fantasy
* Start reading here: Stone and Scepter, will be published in April.

### Kosta

* Website: https://voidbrood.neocities.org
* Fediverse Profile: https://eldritch.cafe/@spacetrash
* Genre: Hard SF
* Start reading here: There are two short fiction stories available at https://voidbrood.neocities.org/library

### Kurt Hohmann

* Website: https://www.kurthohmann.com
* Fediverse Profile: https://mastodon.world/@KurtHohmann
* Genre: Fiction
* Start reading here: There are several short stories at https://www.kurthohmann.com/writing/

### Leonid Korogodski

* Website: https://www.pinknoise.net
* Fediverse Profile: https://writing.exchange/@cyberhuman
* Genre: SF
* Start reading here:
    * Pink Noise: A Posthuman Tale (2010)
    * He is currently working on a series of novels. A brief pitch for Book 1, "The Hornets' Nest":  
*When our minds can access cyberspace subconsciously, the world turns magical—but one can also kill or maim with thought. THE HORNETS' NEST centers trauma and the healing power of friendship. Four friends work their way through generations of trauma from war and xenophobia, forming more than just a team: a family. The story explores music as a language of emotions and dreams as the internal psychotherapy of mind—and asks if terrorism can ever be a justified response, even to genocide.*

### Matthew Graybosch

* Website: https://starbreaker.org/
* Fediverse Profile: https://indieweb.social/@starbreaker
* Genre: science fantasy, heavy metal, cyberpunk, conspiracy, post-apocalypse
* All work freely available online at <https://starbreaker.org/fiction/>
* License: Creative Commons Attribution-NonCommercial-ShareAlike 4.0
* Start reading here:
  * ["The Milgram Battery"](https://starbreaker.org/fiction/stories/milgram-battery/)
  * [*Without Bloodshed*](https://starbreaker.org/fiction/novels/without-bloodshed/)
* WIP:
  * [*Spiral Architect*](https://starbreaker.org/fiction/wip/spiral-architect/): *When attempts by a biotech startup to claim artificial humanoids as corporate property draw the Phoenix Society's attention, they put Morgan Cooper on the case even though he's supposed to be on vacation. That alone would make it personal, but Morgan himself is a potential target. When the law is no shield, only the sword remains.*

### Naldela Isur'Ra Teleyal

* Website: https://teleyal.blog
* Fediverse Profile: https://tavern.drow.be/@naldela@teleyal.blog
* Genre: Fantasy
* Start reading here: On the last page of the journal on the website.

### Nilly Robot

* Website: https://nillyrobot.com
* Fediverse Profile: https://mastodon.art/@nillyrobot
* Genre: Horror-comedy and suspense
* Start reading here:
    * The teaser comic linked on the website or microfiction on https://dotart.blog/nillyrobot/ for more traditional writing.
    * The WIPs for Writing Wonders are "Jupiter Lane," a horror/comedy comic and "Phantasmagory," a suspense/horror novella 

### Odessa Silver

* Website: https://linktr.ee/odessasilver
* Fediverse Profile: https://mastodonapp.uk/@odessasilver
* Genre: Fantasy
* Start reading here: "Tales of Yamamoto: Hotaka", see story section on linktree.

### Pax Asteriae

* Website: https://www.paxasteriae.co.uk
* Fediverse Profile: https://writing.exchange/@PaxAsteriae
* Genre: Steampunk, modern fantasy, fantasy, Sci-Fi, Romance
* Start reading here: [The Fair Automaton](https://www.paxasteriae.co.uk/steampunk/)

### Pete Alex Harris

* Website: https://torn-and-crumpled.page
* Fediverse Profile: https://mastodon.scot/@petealexharris
* Genre: SFF and horror, short stories are mixed-genre
* Start reading here: Short story collection [Silk and Sharp Edges](https://www.smashwords.com/books/view/1041554) which is available for free with the coupon code UP53U (expires June 1st 2023).

### Riza Bunanasa

* Website: n/a
* Fediverse Profile: https://mastodon.online/@rizabunanasa
* Genre: Gaslamp fantasy, modern fantasy, isekai
* Start reading here: Not published yet.

### Sarah J Hoodlet

* Website: n/a
* Fediverse Profile: https://mastodon.social/@SJHoodlet
* Genre: Fantasy, fantasy romance, speculative fiction
* Start reading here: Not published yet.

### Sazzle

* Website: n/a
* Fediverse Profile: https://romancelandia.club/@Sazzle
* Genre: Contemporary high steam queer romance with a paranormal edge
* Start reading here: Not published yet, there are 2 WIPs, "Scratch the Surface" and "Denial"

### Sienna Eggler

* Website: https://www.siennaeggler.com
* Fediverse Profile: https://writing.exchange/@benetnasch
* Genre: Sapphic paranormal romance, sci-fi, and urban fantasy
* Start reading here: The debut [Fluid Bonding](https://books2read.com/fluidbonding)

### Steve Turnbull

* Website: https://taupress.com/author/steve-turnbull-1
* Fediverse Profile: https://mastodonapp.uk/@adaddinsane
* Genre: Fantasy, SF and Steampunk books, also erotica, with LGBTQ+ and diverse leads
* Start reading here: [Rebel Dragon](https://taupress.com/book/rebel-dragon-4)

### Strange Seawolf

* Website: https://fly.herald-petrel.com/
* Fediverse Profile: https://mindly.social/@strangeseawolf
* Genre: Sci-Fi
* Start reading here: [The Rebirth of the Dragon Child 2521](https://archiveofourown.org/works/45910072)
* Notes: If you want to get updated on new blog posts on the project you can follow @admin@fly.herald-petrel.com

### Wandering Beekeeper

* Website: https://wheretofind.me/@TarlimanJoppos
* Fediverse Profile: https://wandering.shop/@WanderingBeekeeper
* Genre: Historical fiction, goofy fantasy
* Start reading here:
    * Breadspotting, Updates on [Patreon](https://www.patreon.com/wanderingbeekeeper). It's a mid 1800s historical setting. Think Peaky Blinders meets Victorian Bakers. A small family bakery and its gang of drivers and loaders go up against a big firm muscling its way into a working poor neighborhood.
    * [Time Tripper](https://archiveofourown.org/series/3133971), a Time Bandits fanfic

## Outro

This list was created by [@marcr@social.tchncs.de](https://social.tchncs.de/@marcr). If you are participating in #WritingWonders and want to be added, or want an existing entry of yours to be changed, just message me. Alternatively you can also open an issue or PR here.
